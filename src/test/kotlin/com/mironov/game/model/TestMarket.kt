package com.mironov.game.model

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 25.02.2019
 */

class TestMarket {

    private val market = Market()

    @Test
    fun testQualityPercentilesCalculating() {
        fillMarketProductsStandard()
        println(market.getMinQuality())
        println(market.getLowQuality())
        println(market.getMediumQuality())
        println(market.getHighQuality())
        println(market.getMaxQuality())
    }

    @Test
    fun testNormalSelling() {
        fillMarketProductsStandard()
        market.fillMarketDemand()
        val result = market.sellProducts()
        for (product in result.keys) {
            println("Product " + product.name + " earned " + result[product])
            assertNotEquals(0.0, result[product])
        }
    }

    @Test
    fun testSellingOneProductNotQualified() {
        fillMarketProductsStandard()
        val offProduct = Product("Too pricy", "ololo", 50.0, 49)
        market.addProducts(offProduct)
        market.fillMarketDemand()

        val result = market.sellProducts()
        assertEquals(0.0, result[offProduct])
        for (product in result.keys) {
            println("Product " + product.name + " earned " + result[product])
        }
    }

    private fun fillMarketProductsStandard() {
        var i = 0
        while (i < 90) {
            val quality = i + 1
            val price = quality.toDouble()
            val name = "Product with quality $quality and price $price"
            market.addProducts(Product(name, "testAuthor", price, quality))
            i++
        }
    }

    private fun printGistograph(value: Int) {
        val cap = 20_000
        val percent: Int = (value * 10) / cap
        when (percent) {
            0 -> println("*")
            1 -> println("**")
            2 -> println("***")
            3 -> println("****")
            4 -> println("*****")
            5 -> println("******")
            6 -> println("*******")
            7 -> println("********")
            8 -> println("*********")
            9 -> println("**********")
            10 -> println("**********")
            else -> println("TOO MUCH")
        }

    }

}