package com.mironov.game.model

import org.mockito.Mockito
import org.mockito.Mockito.mock

/**
 * Created by Daniel Mironov on 16.02.2019
 */

fun getTestEmployeesPayGradeSum(): Double {
    return getTestEmployees().map { it.payGrade }.sum()
}

fun getTestEmployeesLevelSum(): Int {
    return getTestEmployees().map { it.level }.sum()
}

fun getTestEmployeesCount(): Int {
    return getTestEmployees().size
}

fun getTestEmployees(): List<Employee> {
    val daniel = Employee("Daniel", 50.0, 20)
    val bob = Employee("Bob", 40.0, 15)
    val igor = Employee("Igor", 30.0, 10)
    val arthur = Employee("Arthur", 20.0, 5)
    return arrayListOf(daniel, bob, igor, arthur)
}

fun getMockedProjects(): List<Project> {
    val project1: Project = mock(Project::class.java)
    Mockito.`when`(project1.isFinished).thenReturn(false)
    val project2: Project = mock(Project::class.java)
    Mockito.`when`(project2.isFinished).thenReturn(false)
    val project3: Project = mock(Project::class.java)
    Mockito.`when`(project3.isFinished).thenReturn(false)
    return arrayListOf(project1, project2, project3)
}

fun getMockedCompanies(): List<Company> {
    val company1: Company = mock(Company::class.java)
    val company2: Company = mock(Company::class.java)
    val company3: Company = mock(Company::class.java)
    return arrayListOf(company1, company2, company3)

}

fun getMockedUI(): UIDisplayGate {
    return mock(UIDisplayGate::class.java)
}
