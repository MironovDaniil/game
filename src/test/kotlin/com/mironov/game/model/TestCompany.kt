package com.mironov.game.model

/**
 * Created by Daniel Mironov on 16.02.2019
 */

import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import java.util.*

class TestCompany {
    private val money = 20000.0
    private val rent = 30.0
    private val basicNeedsCoeff = 10.0
    private val extraNeedsCoeff = 15.0

    private var company: Company = Company(
            "TestCompany",
            money,
            rent,
            basicNeedsCoeff,
            extraNeedsCoeff,
            getTestEmployees().toMutableList(),
            getMockedProjects().toMutableList())

    @Test
    fun testNextDay() {
        val billExpected = getTestEmployeesPayGradeSum() + rent +
                +getTestEmployeesCount() * (basicNeedsCoeff + extraNeedsCoeff)
        val moneyExpected = money - billExpected
        company.nextDay()
        assertEquals(moneyExpected, company.money, 0.01)
    }
}
