package com.mironov.game.model

import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test

/**
 * Created by Daniel Mironov on 20.02.2019
 */

class TestMath {
    @Test
    fun testPercentileCalculatingFromZeroToTen() {
        val list = listOf(5, 9, 3, 10, 2, 6, 4, 7, 8, 1)
        assertEquals(0, percentile(list, 0.0))
        assertEquals(2, percentile(list, 0.25))
        assertEquals(5, percentile(list, 0.5))
        assertEquals(6, percentile(list, 0.6))
        assertEquals(10, percentile(list, 1.0))
    }

    @Test
    fun testPercentileCalculatingFromZeroToHundred() {
        val list = emptyList<Int>().toMutableList()
        var i = 0
        while (i < 100) {
            list.add(i + 1)
            i++
        }
        assertEquals(0, percentile(list, 0.0))
        assertEquals(1, percentile(list, 0.01))
        assertEquals(2, percentile(list, 0.02))
        assertEquals(100, percentile(list, 1.0))
        assertEquals(50, percentile(list, 0.5))
        assertEquals(25, percentile(list, 0.25))
        assertEquals(75, percentile(list, 0.75))
    }

    @Test(expected = IllegalArgumentException::class)
    fun testLessThanZeroPercentile() {
        val list = listOf(5, 9, 3, 10, 2, 6, 4, 7, 8, 1)
        percentile(list, -1.0)
        fail()
    }

    @Test(expected = IllegalArgumentException::class)
    fun testMoreThanOnePercentile() {
        val list = listOf(5, 9, 3, 10, 2, 6, 4, 7, 8, 1)
        percentile(list, 2.0)
        fail()
    }
}