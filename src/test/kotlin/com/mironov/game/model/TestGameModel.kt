package com.mironov.game.model

import com.mironov.game.dto.GameDTO
import com.mironov.game.dto.InstructionsDTO
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.*

/**
 * Created by Daniel Mironov on 19.02.2019
 */

class TestGameModel {

    private var game = GameModel(
            getMockedCompanies()[0],
            getMockedCompanies().toMutableList(),
            0,
            getMockedUI()
    )

    @Test
    fun testNextDay() {
        game.nextDay()
        for (company in game.companies) {
            verify(company, times(1)).nextDay()
        }
        assertEquals(1, game.time)
    }

    @Test
    fun testDisplay() {
        val ui = game.ui
        val gameDTO = GameDTO(0)
        game.display()
        verify(ui, times(1)).display(gameDTO)
    }

    @Test
    fun testApplyInstructions() {
        val instructionsDTO = InstructionsDTO()
        Mockito.`when`(game.ui.getInstructions()).thenReturn(instructionsDTO)
        game.applyInstructions()
        verify(game.playerCompany, times(1)).applyInstructions(instructionsDTO)
    }
}