package com.mironov.game.model

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 18.02.2019
 */

class TestProject {

    private var project = Project("Test project", getTestEmployees().toMutableList(), 10)

    @Test
    fun testNextDayCalculating() {
        var i = 0
        while (i < 10) {
            assertEquals(0, project.getQualityPoints())
            project.nextDay()
            i++
        }
        assertEquals(500, project.getQualityPoints())

    }

    @Test
    fun testNextDayCalculatingAddEmployeeInTheMiddleAndPostponing() {
        val carl = Employee("Carl", 30.0, 15)
        var i = 0
        while (i < 20) {
            assertEquals(0, project.getQualityPoints())
            project.nextDay()
            if (i == 5) {
                project.assignEmployee(carl)
                project.postponeBy(10)
            }
            i++
        }
        assertEquals(1210, project.getQualityPoints())
    }

    @Test
    fun testNextDayCalculatingAndDisavowing() {
        val bob = getTestEmployees()[1]
        var i = 0
        while (i < 10) {
            assertEquals(0, project.getQualityPoints())
            project.nextDay()
            if (i == 2) {
                project.disavowEmployee(bob)
            }
            i++
        }
        assertEquals(395, project.getQualityPoints())
    }

}