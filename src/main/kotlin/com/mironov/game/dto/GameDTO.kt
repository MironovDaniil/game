package com.mironov.game.dto

/**
 * Created by Daniel Mironov on 18.02.2019
 */

data class GameDTO (val time: Long)