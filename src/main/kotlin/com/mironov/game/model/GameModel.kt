package com.mironov.game.model

import com.mironov.game.dto.GameDTO
import com.mironov.game.dto.InstructionsDTO
import com.mironov.game.dto.InstructionsRequestDTO

/**
 * Created by Daniel Mironov on 18.02.2019
 */

class GameModel(playerCompany: Company,
                market: Market,
                companies: MutableList<Company>,
                time: Long,
                uiDisplay: UIDisplayGate,
                uiInstructions: UIInstructionsGate) {

    var playerCompany = playerCompany
        private set
    var market = market
        private set
    var companies = companies
        private set
    var time = time
        private set
    var ui = uiDisplay
        private set
    var uiInstructions = uiInstructions
        private set

    fun nextDay() {
        market.fillMarketDemand()
        val sellingResult = market.sellProducts()
        for (company in companies) {
            val income = company.releasedProducts.map { sellingResult.getOrDefault(it, 0.0) }.sum()
            company.nextDay(income)
            val request = company.formInstructionsRequest()
            company.applyInstructions(getInstructions(request))
            market.addProducts(company.releasedProducts)
        }
        time++
    }

    private fun getInstructions(instructionsRequest: InstructionsRequestDTO)
            : InstructionsDTO = uiInstructions.getInstructions(instructionsRequest)

    fun display() {
        val gameDTO = GameDTO(time)
        ui.display(gameDTO)
    }
}