package com.mironov.game.model

import java.util.*
import kotlin.IllegalArgumentException


/**
 * Created by Daniel Mironov on 20.02.2019
 */

fun  percentile(list: List<Int>, percent: Double): Int {
    if ((percent < 0) || (percent > 1)) {
        throw IllegalArgumentException("Percent must satisfy 0 < percent < 1 !")
    }
    val sortedList = list.sorted()
    val index: Int = (sortedList.size * percent).toInt() - 1
    return if (index >= 0) sortedList[index] else (sortedList[0] - 1)
}

fun gauss(middle: Double, deviation: Double): Double {
    val random = Random()
    val standardGauss = random.nextGaussian()
    return (standardGauss * deviation) + middle

}