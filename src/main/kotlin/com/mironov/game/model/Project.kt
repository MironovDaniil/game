package com.mironov.game.model

/**
 * Created by Daniel Mironov on 17.02.2019
 */

class Project(val name: String,
              private var assignedEmployees: MutableList<Employee>,
              private var timeUntilFinished: Long) {
    private var qualityPoints: Int = 0
    var isFinished = false
        private set

    fun nextDay() {
        if (isFinished)
            return
        develop()
        timeUntilFinished--
        checkFinish()
    }

    private fun develop() {
        for (employee in assignedEmployees) {
            qualityPoints += employee.level
        }
    }

    private fun checkFinish() {
        if (timeUntilFinished == 0L) {
            isFinished = true
        }
    }

    fun getQualityPoints(): Int {
        return if (isFinished) {
            qualityPoints
        } else {
            0
        }
    }

    fun assignEmployee(employee: Employee) {
        assignedEmployees.add(employee)
    }

    fun disavowEmployee(employee: Employee) {
        assignedEmployees.remove(employee)
    }

    fun postponeBy(postponeAmount: Long) {
        timeUntilFinished += postponeAmount
    }

}