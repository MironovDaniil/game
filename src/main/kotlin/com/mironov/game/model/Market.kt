package com.mironov.game.model

/**
 * Created by Daniel Mironov on 19.02.2019
 */

class Market {

    private var marketSize = 1_000_000

    private val products: MutableList<Product> = emptyList<Product>().toMutableList()

    fun getProducts(): List<Product> {
        return products.toList()
    }

    private val demandList: IntArray = IntArray(100) { 0 }

    fun addProducts(products: Collection<Product>) {
        products.forEach { this.products.add(it) }
    }

    fun addProducts(vararg products: Product) {
        for (product in products) {
            this.products.add(product)
        }
    }

    fun getMinQuality(): Int {
        return qualityList().min() ?: 0
    }

    fun getMaxQuality(): Int {
        return qualityList().max() ?: 0
    }

    fun getMediumQuality(): Int {
        return percentile(qualityList(), 0.5)
    }

    fun getLowQuality(): Int {
        return percentile(qualityList(), 0.25)
    }

    fun getHighQuality(): Int {
        return percentile(qualityList(), 0.75)
    }

    fun fillMarketDemand() {
        var i = 0
        while (i < marketSize) {
            val qualityDemand = getRandomQualityDemand()
            demandList[qualityDemand]++
            i++
        }
    }

    private fun getRandomQualityDemand(): Int {
        var gaussRandom = gauss(50.0, 20.0)
        if (gaussRandom < 0) gaussRandom = 0.0
        if (gaussRandom > 100) gaussRandom = 99.0
        return gaussRandom.toInt()
    }

    fun sellProducts(): Map<Product, Double> {
        val result: MutableMap<Product, Double> = emptyMap<Product, Double>().toMutableMap()
        for (product in products) {
            result[product] = 0.0
        }
        var i = 0
        while (i < 100) {
            val demandAmount = demandList[i]
            val qualityThreshold = getQualityThresholdOutOfIndex(i)
            val optimalProducts = getOptimalProducts(qualityThreshold)
            val copiesToEach = demandAmount / optimalProducts.size
            for (product in optimalProducts) {
                val cashAlready = result[product]
                if (cashAlready == null) {
                    result[product] = copiesToEach * product.price
                } else {
                    result[product] = cashAlready + copiesToEach * product.price
                }
            }
            i++
        }
        return result.toMap()
    }

    private fun getQualityThresholdOutOfIndex(index: Int): Int {
        return when (index) {
            0 -> getMinQuality()
            99 -> getMaxQuality()
            else -> {
                val percent: Double = (index + 1) / 100.0
                getPercentileQuality(percent)
            }
        }
    }

    private fun getPercentileQuality(percent: Double): Int {
        return percentile(qualityList(), percent)
    }

    private fun getOptimalProducts(qualityThreshold: Int): List<Product> {
        val availableProducts = getProductsWithEnoughQuality(qualityThreshold)
        val minAvailablePrice: Double = availableProducts.map { it.price }.min() ?: 0.0
        if (minAvailablePrice == 0.0) return emptyList()
        return getProductsWithMaxQualityOfGivenPrice(minAvailablePrice)
    }

    private fun getProductsWithEnoughQuality(quality: Int): List<Product> {
        return products.filter { it.quality >= quality }
    }

    private fun getProductsWithMaxQualityOfGivenPrice(price: Double): List<Product> {
        val initProducts = getProductsWithGivenPrice(price)
        val quality = initProducts.map { it.quality }.max() ?: 0
        return initProducts.filter { it.quality == quality }
    }

    private fun getProductsWithGivenPrice(price: Double): List<Product> {
        return products.filter { it.price == price }
    }

    private fun qualityList(): List<Int> {
        return products.map { it.quality }
    }

}