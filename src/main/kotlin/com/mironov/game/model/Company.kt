package com.mironov.game.model

import com.mironov.game.dto.InstructionsDTO
import com.mironov.game.dto.InstructionsRequestDTO
import java.lang.IllegalArgumentException

/**
 * Created by Daniel Mironov on 16.02.2019
 */

class Company(val companyName: String,
              money: Double,
              rent: Double,
              val basicNeedsCoefficient: Double,
              extraNeedsCoefficient: Double,
              employees: MutableList<Employee>,
              projects: MutableList<Project>) {

    var money = money
        private set
    var rent = rent
        private set
    var extraNeedsCoefficient = extraNeedsCoefficient
        private set
    var employees = employees
        private set
    var projects = projects
        private set
    private var finishedProjects: MutableList<Project> = emptyList<Project>().toMutableList()
    var releasedProducts: MutableList<Product> = emptyList<Product>().toMutableList()
        private set


    fun formInstructionsRequest(): InstructionsRequestDTO {
        return InstructionsRequestDTO()
    }



    fun applyInstructions(instructionsDTO: InstructionsDTO) {
        for (project in finishedProjects) {
            val price = instructionsDTO.productPrices[project.name]
            if (price != null) {
                releaseProject(project, price)
            }
        }
    }

    fun nextDay(income: Double) {
        money += income
        prepareAllAvailableProjectsForRelease()
        advanceAllProjects()
        payBills()
    }

    private fun advanceAllProjects() {
        projects.forEach { it.nextDay() }
    }

    private fun prepareAllAvailableProjectsForRelease() {
        projects.filter { it.isFinished }.forEach {
            finishedProjects.add(it)
            projects.remove(it)
        }
    }

    private fun releaseProject(project: Project, price: Double) {
        if (!project.isFinished)
            throw IllegalArgumentException()
        finishedProjects.remove(project)
        val product = mapProjectToProduct(project, companyName, price)
        releasedProducts.add(product)
    }

    private fun payBills() {
        money -= calculateBill()
    }

    private fun calculateBill(): Double {
        var totalBill = 0.0
        for (employee in employees) {
            totalBill += employee.payGrade
        }
        totalBill += rent
        totalBill += (basicNeedsCoefficient * employees.size)
        totalBill += (extraNeedsCoefficient * employees.size)
        return totalBill
    }


}