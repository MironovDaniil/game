package com.mironov.game.model

/**
 * Created by Daniel Mironov on 16.02.2019
 */

data class Employee (val name: String, var payGrade: Double, var level: Int)