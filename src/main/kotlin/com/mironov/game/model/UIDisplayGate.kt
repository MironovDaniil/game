package com.mironov.game.model

import com.mironov.game.dto.GameDTO
import com.mironov.game.dto.InstructionsDTO

/**
 * Created by Daniel Mironov on 18.02.2019
 */

interface UIDisplayGate {
    fun display(gameDTO: GameDTO)
}