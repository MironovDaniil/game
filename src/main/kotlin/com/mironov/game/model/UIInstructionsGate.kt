package com.mironov.game.model

import com.mironov.game.dto.InstructionsDTO
import com.mironov.game.dto.InstructionsRequestDTO

/**
 * Created by Daniel Mironov on 27.02.2019
 */

interface UIInstructionsGate {
    fun getInstructions(instructionsRequest: InstructionsRequestDTO): InstructionsDTO
}