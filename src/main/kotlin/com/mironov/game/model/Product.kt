package com.mironov.game.model

/**
 * Created by Daniel Mironov on 19.02.2019
 */

data class Product (val name: String,
                    val authorName: String,
                    val price: Double,
                    val quality: Int)