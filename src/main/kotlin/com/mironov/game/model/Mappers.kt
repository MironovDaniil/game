package com.mironov.game.model

import java.lang.IllegalArgumentException

/**
 * Created by Daniel Mironov on 19.02.2019
 */

fun mapProjectToProduct(project: Project, authorName: String, price: Double): Product {
    return if (project.isFinished) {
        Product(project.name, authorName, price, project.getQualityPoints())
    } else {
        throw IllegalArgumentException()
    }
}